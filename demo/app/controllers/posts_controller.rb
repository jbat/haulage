class PostsController < ApplicationController
  def show
    @akash = Post.find(params[:id])
  end

  def new
    @akash = Post.new
  end

  def create
    @akash = Post.new(params[:post].permit(:title, :text))

    @akash.save
    redirect_to @akash
  end

  def index
    @akash = Post.all
  end

end
