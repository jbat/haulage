# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140420150530) do

  create_table "booking_details", force: true do |t|
    t.integer  "branch_id"
    t.integer  "gr_no"
    t.integer  "consignor_id"
    t.integer  "consignee_id"
    t.string   "source"
    t.string   "destination"
    t.string   "consignment_status"
    t.string   "payment_status"
    t.boolean  "is_dalal"
    t.text     "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "branch_infos", force: true do |t|
    t.integer  "firm_id"
    t.string   "code"
    t.string   "name"
    t.string   "city"
    t.string   "poc"
    t.string   "address"
    t.integer  "under_branch"
    t.string   "service_tax_no"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "consignment_breakdown_details", force: true do |t|
    t.integer  "consignment_id"
    t.integer  "split_consignment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "consignment_details", force: true do |t|
    t.integer  "branch_id"
    t.integer  "gr_no"
    t.integer  "challan_id"
    t.boolean  "is_broken"
    t.string   "type"
    t.integer  "quantity"
    t.string   "name"
    t.float    "weight"
    t.float    "rate"
    t.float    "amount"
    t.string   "private_marka"
    t.string   "created_by"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "firm_infos", force: true do |t|
    t.string   "name"
    t.string   "poc"
    t.string   "phone"
    t.string   "email"
    t.text     "address"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "possible_expenses", force: true do |t|
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id"
  end

  create_table "truck_infos", force: true do |t|
    t.integer  "firm_id"
    t.string   "truck_no"
    t.string   "owner"
    t.string   "passing_date"
    t.string   "permit_number"
    t.datetime "fitness_date"
    t.integer  "hub_grease_km"
    t.datetime "oil_grease_km"
    t.string   "truck_type"
    t.datetime "insurance_date"
    t.float    "starting_km"
    t.boolean  "is_on_rent"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "user_infos", force: true do |t|
    t.integer  "branch_id"
    t.string   "login_id"
    t.string   "password"
    t.string   "mobile"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "branch_id"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
