class CreateFirmInfos < ActiveRecord::Migration
  def change
    create_table :firm_infos do |t|
      t.string :name
      t.string :poc
      t.string :phone
      t.string :email
      t.text :address

      t.timestamps
    end
  end
end
