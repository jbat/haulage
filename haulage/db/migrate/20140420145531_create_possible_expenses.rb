class CreatePossibleExpenses < ActiveRecord::Migration
  def change
    create_table :possible_expenses do |t|
      t.string :type

      t.timestamps
    end
  end
end
