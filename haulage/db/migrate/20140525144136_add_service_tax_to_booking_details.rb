class AddServiceTaxToBookingDetails < ActiveRecord::Migration
  def change
    add_column :booking_details, :service_tax, :decimal
  end
end
