class CreateConsignmenrBreakdownDetails < ActiveRecord::Migration
  def change
    create_table :consignmenr_breakdown_details do |t|
      t.integer :consignment_id
      t.integer :split_consignment_id

      t.timestamps
    end
  end
end
