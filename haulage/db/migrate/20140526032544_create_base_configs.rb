class CreateBaseConfigs < ActiveRecord::Migration
  def change
    create_table :base_configs do |t|
      t.string :type
      t.string :value

      t.timestamps
    end
  end
end
