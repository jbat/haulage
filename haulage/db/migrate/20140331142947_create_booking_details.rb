class CreateBookingDetails < ActiveRecord::Migration
  def change
    create_table :booking_details do |t|
      t.integer :branch_id
      t.integer :gr_no
      t.integer :consignor_id
      t.integer :consignee_id
      t.string :source
      t.string :destination
      t.string :consignment_status
      t.string :payment_status
      t.boolean :is_dalal
      t.text :description

      t.timestamps
    end
  end
end
