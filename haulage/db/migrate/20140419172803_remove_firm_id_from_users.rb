class RemoveFirmIdFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :firm_id, :integer
  end
end
