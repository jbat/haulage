class AddTotalAmountToBookingDetails < ActiveRecord::Migration
  def change
    add_column :booking_details, :total_amount, :decimal
  end
end
