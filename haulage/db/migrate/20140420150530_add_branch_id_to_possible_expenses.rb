class AddBranchIdToPossibleExpenses < ActiveRecord::Migration
  def change
    add_column :possible_expenses, :branch_id, :integer
  end
end
