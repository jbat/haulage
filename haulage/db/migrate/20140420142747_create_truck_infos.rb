class CreateTruckInfos < ActiveRecord::Migration
  def change
    create_table :truck_infos do |t|
      t.integer :firm_id
      t.string :truck_no
      t.string :owner
      t.string :passing_date
      t.string :permit_number
      t.datetime :fitness_date
      t.integer :hub_grease_km
      t.datetime :oil_grease_km
      t.string :truck_type
      t.datetime :insurance_date
      t.float :starting_km
      t.boolean :is_on_rent

      t.timestamps
    end
  end
end
