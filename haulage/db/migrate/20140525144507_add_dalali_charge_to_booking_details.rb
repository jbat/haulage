class AddDalaliChargeToBookingDetails < ActiveRecord::Migration
  def change
    add_column :booking_details, :dalali_charge, :decimal
  end
end
