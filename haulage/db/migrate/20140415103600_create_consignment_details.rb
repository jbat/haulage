class CreateConsignmentDetails < ActiveRecord::Migration
  def change
    create_table :consignment_details do |t|
      t.integer :branch_id
      t.integer :gr_no
      t.integer :challan_id
      t.boolean :is_broken
      t.string :type
      t.integer :quantity
      t.string :name
      t.float :weight
      t.float :rate
      t.float :amount
      t.string :private_marka
      t.string :created_by

      t.timestamps
    end
  end
end
