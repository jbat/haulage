class AddLabourChargeToBookingDetails < ActiveRecord::Migration
  def change
    add_column :booking_details, :labour_charge, :decimal
  end
end
