class CreateBranchInfos < ActiveRecord::Migration
  def change
    create_table :branch_infos do |t|
      t.integer :firm_id
      t.string :code
      t.string :name
      t.string :name
      t.string :city
      t.string :poc
      t.string :address
      t.integer :under_branch
      t.string :service_tax_no

      t.timestamps
    end
  end
end
