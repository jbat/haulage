class TruckManagerController < ApplicationController
  respond_to :html, :json
  skip_before_filter :verify_authenticity_token

  def new
    TruckInfo.new

  end
  def create
    params.permit!
    @truckManager = TruckInfo.create(params['truckManager'])
    @truckManager.firm_id=session[:firmId]
    @truckManager.save
    respond_with({status: 'Entry has been added successfully'}, location: '/branch_manager/new')
  end
end
