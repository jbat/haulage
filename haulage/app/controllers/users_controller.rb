class UsersController < ApplicationController

  respond_to :html, :json
  skip_before_filter :verify_authenticity_token
  skip_before_filter :require_login, :only => [:login, :signIn]

  def signIn
    if !:user.empty?
      @user = UserInfo.find_by_login_id_and_password(params[:user][:login_id],params[:user][:password])
      if !@user.nil?
        session[:current_user_id] = @user.id
        redirect_to :back
      else
        flash[:notice] = "Invalid Username or Password! Please try again."
        render 'login'
      end
    else
      render 'login'
    end
  end

  def login
    @user=UserInfo.new
  end

  def new
    @user = UserInfo.new

  end

  def create
    @user = UserInfo.new(post_params)
    if @user.save
      #respond_with(@user)
      redirect_to :back
    else
      flash[:notice] = "Invalid form"
      #redirect_to :controller => 'users', :action => 'login'
      render 'login'
    end

  end

  def show
    @user = UserInfo.all
    respond_with(@user)
  end

end

