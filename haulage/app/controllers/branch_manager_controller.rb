class BranchManagerController < ApplicationController
  respond_to :html, :json
  skip_before_filter :verify_authenticity_token

  def new
    BranchInfo.new

  end
  def create
    params.permit!
    @branchManager = BranchInfo.create(params['branchManager'])
    @branchManager.firm_id=session[:firmId]
    @branchManager.save
    respond_with({status: 'Entry has been added successfully'}, location: '/branch_manager/new')
  end
end
