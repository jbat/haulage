class BookingDetailsController < ApplicationController
  respond_to :html, :json
  skip_before_filter :verify_authenticity_token
  def show

  end

  def get_sources
    @pos = BookingDetails.pluck(:source)
    respond_with({source: @pos}, location:"/recruitment")
  end

  def new
    ConsignmentBreakdownDetails.new
    BookingDetails.new
    ConsignmentDetail.new
  end

  def create
    #pp=params['consignmentBreakDownDetail']
    params.permit!
    @bookingDetail = BookingDetails.create(params['bookingDetail'])
    @bookingDetail.branch_id=session[:branchId]
    @bookingDetail.save
    for c in params['consignmentDetail']
      @consignmentDetail = ConsignmentDetail.create(c)
      @consignmentDetail.gr_no = params['bookingDetail'][:gr_no]
      @consignmentDetail.save
    end
    respond_with({status: 'Entry has been added successfully'}, location: '/booking_details/new')
  end

  private

  def pp
    params.require(:consignmentBreakDownDetail).permit(:consignment_id)
  end

end
