class SessionsController < Devise::SessionsController
  def create
    super
    session[:branchId]=current_user.branch_id
    session[:firmId] = BranchInfo.find(current_user.branch_id).firm_id
  end
end
