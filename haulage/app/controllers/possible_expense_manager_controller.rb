class PossibleExpenseManagerController < ApplicationController
  respond_to :html, :json
  skip_before_filter :verify_authenticity_token

  def new
    PossibleExpenses.new
  end

  def create
    params.permit!
    @possibleExpenseManager = PossibleExpenses.create(params['possibleExpenseManager'])
    @possibleExpenseManager.branch_id=session[:branchId]
    @possibleExpenseManager.save
    respond_with({status: 'Entry has been added successfully'}, location: '/possible_expense_manager/new')
  end

  def update
    params.permit!
    @possibleExpense=PossibleExpenses.find(params[:expense][:id])
    if@possibleExpense.update_attributes(params[:expense])
      respond_with({success:true,statusText:"Updated Successfully!"}, location: '/possible_expense_manager/show')
    else
      respond_with({success:false,statusText:"Update failed!"}, location: '/possible_expense_manager/show')
    end
  end

  def show
  end

  def get_possible_expenses
    @allPossibleExpenses=PossibleExpenses.all
    respond_with({allPossibleExpenses:@allPossibleExpenses}, location: '/possible_expense_manager/show')
  end
end
