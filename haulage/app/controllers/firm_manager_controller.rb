class FirmManagerController < ApplicationController
  respond_to :html, :json
  skip_before_filter :verify_authenticity_token

  def new
    FirmInfo.new

  end
  def create
    params.permit!
    @firmManager = FirmInfo.create(params['firmManager'])
    @firmManager.save
    respond_with({status: 'Entry has been added successfully'}, location: '/branch_manager/new')
  end
end
