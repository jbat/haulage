class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  belongs_to :branch_info
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
