class UserInfo < ActiveRecord::Base
  belongs_to :branch_info
  validates :login_id, presence: true,
                      length:{minimum: 5}

end
