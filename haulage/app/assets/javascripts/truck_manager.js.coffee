truckManager = angular.module("TruckManager",['restangular','angularSlideables']);

truckManager.config ['RestangularProvider',
  (RestangularProvider) ->
    RestangularProvider.setRequestSuffix('.json')
]

truckManager.controller("TruckManagerCtrl", ["$scope", "Restangular", ($scope, $rest ) ->


  $scope.submit_form = () ->
    $rest.one('truck_manager').customPOST({truckManager: $scope.truckManager},'create').then (data) ->
      $scope.data = data.status
])
