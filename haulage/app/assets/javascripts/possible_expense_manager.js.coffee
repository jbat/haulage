possibleExpenseManager = angular.module("PossibleExpenseManager",['restangular','angularSlideables']);

possibleExpenseManager.config ['RestangularProvider',
  (RestangularProvider) ->
    RestangularProvider.setRequestSuffix('.json')
]

possibleExpenseManager.controller("PossibleExpenseManagerCtrl", ["$scope", "Restangular", ($scope, $rest ) ->


  $scope.submit_form = () ->
    $rest.one('possible_expense_manager').customPOST({possibleExpenseManager: $scope.possibleExpenseManager},'create').then (data) ->
      $scope.data = data.status
])



possibleExpenseManager.controller("ShowPossibleExpenseCtrl", ["$scope", "Restangular", ($scope, $rest ) ->
  $rest.one("possible_expense_manager").customPOST({},'get_possible_expenses').then (data) ->
    $scope.possibleExpenses=data.allPossibleExpenses
])


possibleExpenseManager.controller("UpdatePossibleExpenseCtrl", ["$scope", "Restangular", ($scope, $rest ) ->
  $scope.update = () ->
    $rest.one('possible_expense_manager').customPOST({expense: $scope.expense},'update').then (data) ->
      alert(data.statusText)
      if data.success
        $('#makachodi_'+$scope.expense.id).click()
])
