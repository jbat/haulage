firmManager = angular.module("FirmManager",['restangular']);

firmManager.config ['RestangularProvider',
  (RestangularProvider) ->
    RestangularProvider.setRequestSuffix('.json')
]

firmManager.controller("FirmManagerCtrl", ["$scope", "Restangular", ($scope, $rest ) ->


  $scope.submit_form = () ->
    $rest.one('firm_manager').customPOST({firmManager: $scope.firmManager},'create').then (data) ->
      $scope.data = data.status
])
