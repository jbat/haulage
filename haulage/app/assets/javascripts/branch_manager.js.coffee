branchManager = angular.module("BranchManager",['restangular']);

branchManager.config ['RestangularProvider',
  (RestangularProvider) ->
    RestangularProvider.setRequestSuffix('.json')
]

branchManager.controller("BranchManagerCtrl", ["$scope", "Restangular", ($scope, $rest ) ->


  $scope.submit_form = () ->
    $rest.one('branch_manager').customPOST({branchManager: $scope.branchManager},'create').then (data) ->
      $scope.data = data.status
])
