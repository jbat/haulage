bookingDetails = angular.module("BookingDetails",['restangular']);

bookingDetails.config ['RestangularProvider',
  (RestangularProvider) ->
    RestangularProvider.setRequestSuffix('.json')
]



bookingDetails.controller("BookingDetailsCtrl", ["$scope", "Restangular", ($scope, $rest ) ->

  $scope.consignmentDetailList = [{}]

  $rest.one('booking_details').customPOST({},'get_sources').then (data) ->
    $scope.sources = data.source;


  $scope.submit_form = () ->
    $rest.one('booking_details').customPOST({bookingDetail: $scope.bookingDetail,consignmentDetail: $scope.consignmentDetailList},'create').then (data) ->
      $scope.data = data.status

  $scope.addPackage = () ->
    $scope.consignmentDetailList.push({})
  $scope.removePackage = () ->
    $scope.consignmentDetailList.pop({})
])


bookingDetails.controller("PackageCtrl", ["$scope",($scope) ->
  $scope.counter = 1

  $scope.range = (max) ->
    input = []
    i = 1

    while i <= max
      input.push i
      i++
    input


  $scope.addPackage = () ->
    $scope.counter = $scope.counter + 1;
])