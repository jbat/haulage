angular.module('bootstrap-tagsinput', [])
.directive('bootstrapTagsinput', [function() {

  function getItemProperty(scope, property) {
    if (!property)
      return undefined;

    if (angular.isFunction(scope.$parent[property]))
      return scope.$parent[property];

    return function(item) {
      return item[property];
    };
  }

  return {
    restrict: 'EA',
    scope: {
      model: '=model',
      itemvalue: "@",
      itemtext: "@",
      tagClass: "@",
      typeaheadSource: "&"
    },
    template: '<select multiple></select>',
    replace: false,
    link: function(scope, element, attrs) {
      $(function() {
        scope.local_changes = false;
        if (!angular.isArray(scope.model))
          scope.model = [];

        console.log("initing",scope.model);

        var select = $('select', element);
        
        scope.typeaheadSource().promise.then(function(tas){
          var params = {
            typeahead : {
              source   :  tas
            },
            itemValue: scope.itemvalue,
            itemText : scope.itemtext
          };

          console.log("init",scope.model);
          select.tagsinput(params);

          for (var i = 0; i < scope.model.length; i++) {
            select.tagsinput('add', scope.model[i]);
          }

          select.on('itemAdded', function(event) {
            scope.local_changes = true;

            scope.$apply(function(){
              if (scope.model.indexOf(event.item) === -1)
                scope.model.push(event.item);
            });

            scope.local_changes = false;
          });

          select.on('itemRemoved', function(event) {
            scope.local_changes = true;
            var idx = scope.model.indexOf(event.item);
            scope.$apply(function(){
              if (idx !== -1)
                scope.model.splice(idx, 1);
            });
            scope.local_changes = false;
          });

          // create a shallow copy of model's current state, needed to determine
          // diff when model changes
          scope.$watch("model", function() {
            if(scope.local_changes) 
              return;
  
            select.tagsinput('removeAll');
            for (var i = 0; i < scope.model.length; i++) {
              select.tagsinput('add', scope.model[i]);
            }

            // Refresh remaining tags
            select.tagsinput('refresh');

          }, true);
        });
      });
    }
  };
}]);