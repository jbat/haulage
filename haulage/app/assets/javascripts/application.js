// This is a manifest file that'll be compiled into including all the files listed below.
// Add new JavaScript/Coffee code in separate files in this directory and they'll automatically
// be included in the compiled file accessible from http://example.com/assets/application.js
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui-1.9.1.custom.min
//= require angular.min.js
//= require angular-route.min.js
//= require bootstrap-tagsinput.min.js
//= require bootstrap-tagsinput-angular.js
//= require bootstrap-tagsinput.min.js
//= require restangular.js
//= require ui-bootstrap-tpls-0.10.0.min.js
//= require ng-grid-2.0.7.min.js
//= require play
//= require lodash.min.js
//= require_tree .
